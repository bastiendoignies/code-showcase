Bastien DOIGNIES Code Showcase
------------------------------


This code repository contains some multilingual examples of code that I have written in the last years. It is mainly there to support my applications. 

On each branch you will find a project, potentially under development, and a mini description. The current projects are :

* ASCIIDOT : A c-style C++ interpreter for the asciidot esoteric language
* ModelExplorer : An under development python library for machine learning model exploration 
